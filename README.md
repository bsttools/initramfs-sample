A sample buildstream project using BST-Tools to produce a simple initramfs using musl-c and kernel in multiple architectures.

Since this project using bst-toools, it requires a very recent version of buildstream.
I would surgest building buildstream from source using their master branch.

By default, the project will target a generic aarch64/cortex53. you can also build for x86-64 by using the ```arch``` option.

eg:
```
export ARCH=aarch64 # for 64bit arm. default.
export ARCH=x86-64  # for 64bit x86
```

The ```all``` element will stage the initramfs and kernel for checkout.
Build the ```all``` element like so:
```
./bst.sh --max-jobs=`nproc` -o arch ${ARCH} build all.bst
```

Checkout the files:
```
./bst.sh --max-jobs=`nproc` -o arch ${ARCH} artifact checkout --directory=checkout all.bst
```

Run the built kernel and initramfs with QEMU using the generated launch script:
```
./checkout/launch.sh
```

To terminate QEMU; press ```ctrl+a c``` to get to the qemu console, and enter ```quit```.

