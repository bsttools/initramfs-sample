#!/bin/bash
set -e

# Look for downloaded buildstream appimages.
# Look for locally installed buildstreams of the correct version for this project
# If neither exists download a pre-built appimage with curl
# set ${bst} to one of the above and pass the script's arguments to it

appimage_url="https://artifact.bst.tools/buildstream.appimage/buildstream.AppImage"
appimage_file="./buildstream.AppImage"

function dlw_curl () {

	curl ${appimage_url} --output ${appimage_file}

}

bst=0;

if [ -f "${appimage_file}" ]; then

	bst="${appimage_file}"

elif command -v bst2 &> /dev/null; then

	bst=`command -v bst2`

else

	dlw_curl
	chmod +x "${appimage_file}"
	bst="${appimage_file}"

fi

(exec -a "$0" "${bst}" "$@")
exit "$?"
